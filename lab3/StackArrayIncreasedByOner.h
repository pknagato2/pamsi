#pragma once
#include "StackArray.h"

template<class Type>
class StackArrayIncreasedByOne:public StackArray<Type>
{
public:
	void enlargeStack();
	void delargeStack();
	Type remove();

};

template <class Type>
void StackArrayIncreasedByOne<Type>::enlargeStack()
{
	Type *tmp = new Type[size];
	for (int i = 0; i < size; i++)//przepisanie do tymczasowej tablicy
		tmp[i] = ArrayContainer[i];
	delete[] ArrayContainer;
	ArrayContainer = new Type[size +1]; //nowa tablica o 2x wiekszym rozmiarze
	for (int i = 0; i < size; i++)//przepisanie do tymczasowej tablicy
		ArrayContainer[i] = tmp[i];//przepisanie z powrotem
	delete[]tmp; //usuniecie tymczzasowej tablicy
	size += 1;
}

template<class Type>
void StackArrayIncreasedByOne<Type>::delargeStack()
{
		auto tmp = new Type[size -1];
		for (int i = 0; i<size -1; i++)
			tmp[i] = ArrayContainer[i]; //przepisanie do mniejszej tablicy
		delete[] ArrayContainer;
		ArrayContainer = tmp;
		size -= 1;
}

template <class Type>
Type StackArrayIncreasedByOne<Type>::remove()//good
{
	if (isEmpty())
		throw "EmptyStackException";
	//zmniejszanie tablicy
	auto toReturn = ArrayContainer[topIndex--];
	if (topIndex != 0)
			delargeStack();
	return toReturn;
}