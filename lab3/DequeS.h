#pragma once

/*! \brief Klasa reprezentujaca kolejke z dwoma koncami bazujaca na kolejce oraz stosie.
*
*  Wymagania: \n
*	-ListS.h\n
*	-NodeS.h\n
*	-StackS.h\n
*	-QueueS.h\n
*	\n
*  Metody dla stosu :\n
*  -frontAdd()\n
*  -backAdd()\n
*  -remove()\n
*  -removeBack()\n
*  -removeAll()\n
*  -isEmpty()\n
*  -show()\n\n
*
*  -------------Przyklad uzycia:-----------------------\n
*  DequeS<int>* kolejka = new DequeS;\n
*  kolejka->frontAdd(5);\n
*  kolejka->show();\n
*/


#include "ListS.h"
#include "QueueS.h"
#include "StackS.h"

template<class Type>
class DequeS :public QueueS<Type>, public StackS<Type>
{

protected:
	std::string throwEmptyContainerException() throw(std::string) override;
	int Size;
public:
	DequeS();
	bool isEmpty();
	DequeS<Type>* frontAdd(Type toAdd);
	DequeS<Type>* backAdd(Type toAdd);
	Type remove();
	Type removeBack();
	void removeAll();
	void show();
	int size();
	Type getFirstElem() throw(std::string);
	Type getLastElem() throw(std::string);
	NodeS<Type>* getPointerToFirst();
};

template <class Type>
std::string DequeS<Type>::throwEmptyContainerException()
{
	throw "EmptyDequeException";
}

template <class Type>
DequeS<Type>::DequeS()
{
	Size = 0;
}

template <class Type>
bool DequeS<Type>::isEmpty()
{
	return QueueS<Type>::isEmpty();
}

template <class Type>
DequeS<Type>* DequeS<Type>::backAdd(Type toAdd)
{
	Size++;
	QueueS<Type>::backAdd(toAdd);
	return this;
}

template <class Type>
DequeS<Type>* DequeS<Type>::frontAdd(Type toAdd)
{
	Size++;
	StackS<Type>::push(toAdd);
	return this;
}

template <class Type>
Type DequeS<Type>::remove()
{
	Size!=0?Size--:NULL; //jak inaczej sobie poradzi� z tym w przypadku rzucenia wyj�tkiem?
	return StackS<Type>::pop();
}

template <class Type>
Type DequeS<Type>::removeBack()
{
	Type toReturn;
	NodeS<Type>* tmp = first;
	if (first == nullptr)
		throwEmptyContainerException();
	else if (tmp->getNext() == nullptr)
	{
		Size--;
		toReturn = tmp->getElement();
		first = nullptr;
		last = nullptr;
		delete tmp;

	}
	else
	{
		Size--;
		while (tmp->getNext()->getNext() != nullptr)
			tmp = tmp->getNext();
		toReturn = tmp->getNext()->getElement();
		delete tmp->getNext();
		last = tmp;
		tmp->setNext(nullptr);
	}
	return toReturn;
}
template <class Type>
void DequeS<Type>::removeAll()
{
	ListS<Type>::removeAll();
}

template <class Type>
void DequeS<Type>::show()
{
	StackS<Type>::show();
}

template <class Type>
int DequeS<Type>::size()
{
	return Size;
}

template <class Type>
Type DequeS<Type>::getFirstElem() throw(std::string)
{
	if (!isEmpty())
		return first->getElement();
	else
		throwEmptyContainerException();
}

template <class Type>
Type DequeS<Type>::getLastElem() throw(std::string)
{
	if (!isEmpty())
		return last->getElement();
	else
		throwEmptyContainerException();
}

template <class Type>
NodeS<Type>* DequeS<Type>::getPointerToFirst()
{
	return ListS<Type>::getPointerToFirst();
}
