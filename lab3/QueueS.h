#pragma once
#include "ListS.h"

/*! \brief Klasa reprezentujaca kolejke bazujaca na liscie jednokierunkowej.
*
*  Wymagania: \n
*	-ListS.h\n
*	-NodeS.h\n\n
*  Metody dla kolejki: 
*  -Add()\n
*  -remove()\n
*  -removeAll()\n
*  -isEmpty()\n
*  -show()\n\n
*
*  -------------Przyklad uzycia:-----------------------\n
*  QueueS<int>* kolejka = new QueueS;\n
*  kolejka->push(5);\n
*  kolejka->show();\n
*/

template<class Type>
class QueueS:public virtual ListS<Type>
{
protected:
	std::string throwEmptyContainerException() throw(std::string) override;
public:
	ListS<Type>* frontAdd(Type toAdd)=delete;
};

template <class Type>
std::string QueueS<Type>::throwEmptyContainerException() throw(std::string)
{
	throw "EmptyQueueException";
}
