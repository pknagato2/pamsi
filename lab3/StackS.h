#pragma once
#include "ListS.h"

/*! \brief Klasa reprezentujaca stos bazujaca na liscie jednokierunkowej.
*
*  Wymagania: \n
*	-ListS.h\n
*	-NodeS.h\n\n
*  Metody dla stosu :\n
*  -pop()\n
*  -push()\n
*  -popAll()\n
*  -isEmpty()\n
*  -show()\n\n
*
*  -------------Przyklad uzycia:-----------------------\n
*  StackS<int>* stos = new StackS;\n
*  stos->push(5);\n
*  stos->show();\n
*/

template<class Type>
class StackS:public virtual ListS<Type>
{
protected:
	std::string throwEmptyContainerException() throw(std::string)override;
public:
	Type pop();
	Type push(Type toAdd);
	void popAll();

	ListS<Type>* backAdd(Type toAdd)=delete;
	
};

template <class Type>
std::string StackS<Type>::throwEmptyContainerException() throw(std::string)
{
	throw "EmptyStackException";
}

template <class Type>
Type StackS<Type>::pop()
{
	return ListS<Type>::remove();
}

template <class Type>
Type StackS<Type>::push(Type toAdd)
{
	ListS<Type>::frontAdd(toAdd);
	return toAdd;
}

template <class Type>
void StackS<Type>::popAll()
{
	ListS<Type>::removeAll();
}
