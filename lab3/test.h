#pragma once
#include <iostream>
#include <stack>
#include <ctime>

void test(){
	srand(time(NULL));
	int proba[] = { 1000,10000,100000,500000 };
	StackArray<int> stos;
	StackArrayIncreasedByOne<int> stos3;
	std::stack<int>stosSTL;
	StackS<int> stos2;
	timer czas;

	std::ofstream fileToSave("info2.txt", std::ios::out);
	fileToSave << "Wielkosc: ";
	for (int x = 0; x < 4; x++)
	{
		fileToSave << proba[x] << " ";
	}
	fileToSave << "\n";


	fileToSave << "T2: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos.add(rand()); }
			catch (char*msg) { std::cout << msg << std::endl; }
		}
		czas.stop();
		std::cout << "Czas dla tablicy powiekszanej 2x: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "T1: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos3.add(rand()); }
			catch (char*msg) { std::cout << msg << std::endl; }
		}
		czas.stop();
		std::cout << "Czas dla tablicy powiekszanej o 1: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "TS: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stosSTL.push(rand()); }
			catch (char*msg) { std::cout << msg << std::endl; }
		}
		czas.stop();
		std::cout << "Czas dla STL: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "TM: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos2.push(rand()); }
			catch (char*msg) { std::cout << msg << std::endl; }
		}
		czas.stop();
		std::cout << "Czas dla mojego stosu: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";
}
