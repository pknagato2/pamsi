#pragma once
#include <iostream>
#include <windows.h>                // for Windows APIs
using namespace std;

class timer
{
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;			// ticks
	double elapsedTime;

public:
	timer()
	{
		QueryPerformanceFrequency(&frequency);
		elapsedTime = 0;
	}
	void start()
	{
		QueryPerformanceCounter(&t1);
	}
	void stop()
	{
		QueryPerformanceCounter(&t2);
	}
	double getTime()
	{
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		return elapsedTime;
	}
};

