#include <iostream>
#include "time.h"
#include "StackArray.h"
#include "StackArrayIncreasedByOner.h"
#include <stack>
#include "StackS.h"
#include "Hanoi.h"
#include <ctime>
#include <fstream>
using namespace std;

int main()
{
	srand(time(NULL));
	int proba[] = { 1000,10000,100000,500000 };
	StackArray<int> stos;
	StackArrayIncreasedByOne<int> stos3;
	stack<int>stosSTL;
	StackS<int> stos2;
	timer czas;

	std::ofstream fileToSave("info2.txt", std::ios::out);
	fileToSave << "Wielkosc: ";
	for (int x = 0; x < 4; x++)
	{
		fileToSave << proba[x] << " ";
	}
	fileToSave << "\n";


	fileToSave << "T2: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos.add(rand()); }
			catch (char*msg) { cout << msg << endl; }
		}
		czas.stop();
		cout << "Czas dla tablicy powiekszanej 2x: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "T1: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos3.add(rand()); }
			catch (char*msg) { cout << msg << endl; }
		}
		czas.stop();
		cout << "Czas dla tablicy powiekszanej o 1: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "TS: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stosSTL.push(rand()); }
			catch (char*msg) { cout << msg << endl; }
		}
		czas.stop();
		cout << "Czas dla STL: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";

	fileToSave << "TM: ";
	for (int x = 0; x < 4; x++)
	{
		czas.start();
		for (int i = 0; i < proba[x]; i++)
		{
			try { stos2.push(rand()); }
			catch (char*msg) { cout << msg << endl; }
		}
		czas.stop();
		cout << "Czas dla mojego stosu: " << czas.getTime() << " ms.\n";;
		fileToSave << czas.getTime() << " ";
	}
	fileToSave << "\n";
	

	/*StackS<int> stosik,przeznaczenie, przez;

	for (int i = 10; i > 0; i--)
		stosik.push(i);

	moveTower(100,&stosik, &przeznaczenie, &przez);
	showS(przeznaczenie);
	/*

	StackArrayIncreasedByOne<int> stack1;
	cout << "Size: " << stack1.getStackSize() << " IsEmpty: " << stack1.isEmpty() << " IsFull: " << stack1.isFull() << " Stack size: "<<stack1.Size()<<"\n";
	stack1.add(15);
	stack1.add(16);
	stack1.add(17);
	stack1.add(18);
	stack1.add(19);
	cout << "Size: " << stack1.getStackSize() << " IsEmpty: " << stack1.isEmpty() << " IsFull: " << stack1.isFull() << " Stack size: " << stack1.Size() << "\n";
	stack1.remove();
	stack1.remove();
	stack1.remove();
	cout << "Size: " << stack1.getStackSize() << " IsEmpty: " << stack1.isEmpty() << " IsFull: " << stack1.isFull() << " Stack size: " << stack1.Size() << "\n";
	showA(&stack1);
	cout << endl;
	stack1.remove();
	try { stack1.remove(); }
	catch (char* msg) { cout << msg; }
	cout << "Size: " << stack1.getStackSize() << " IsEmpty: " << stack1.isEmpty() << " IsFull: " << stack1.isFull() << " Stack size: " << stack1.Size() << "\n";
	try { stack1.remove(); }
	catch (char* msg) { cout << msg; }
	cout << "Size: " << stack1.getStackSize() << " IsEmpty: " << stack1.isEmpty() << " IsFull: " << stack1.isFull() << " Stack size: " << stack1.Size() << "\n";*/
	cin.get();

	return 0;
}