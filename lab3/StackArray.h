#pragma once
#include <string>

template<class Type>
class StackArray;

template<class Type>
void showA(StackArray<Type>* stos);

template<class Type>
class StackArray
{
protected:
	Type* ArrayContainer;
	int size;
	int topIndex;
public:
	StackArray();
	~StackArray();
	void add(Type toAdd)throw(std::string);
	virtual Type remove()throw(std::string);
	bool isFull();
	bool isEmpty();
	virtual void enlargeStack();
	virtual void delargeStack();
	Type* getFirstPointer();
	int getStackSize();
	int Size(){ return size; }
};

template <class Type> 
StackArray<Type>::StackArray()
{
	size = 1;
	ArrayContainer = new Type[size];
	topIndex =0;
}

template <class Type>
StackArray<Type>::~StackArray()
{
	delete[] ArrayContainer;
}

template <class Type>
bool StackArray<Type>::isFull()
{
	if (topIndex == size)
		return true;
	return false;
}
template <class Type>
bool StackArray<Type>::isEmpty()
{
	if (topIndex <= 0)
		return true;
	return false;
}

template <class Type>
void StackArray<Type>::add(Type toAdd)
{
	if (isFull())
		enlargeStack();

	ArrayContainer[topIndex++] = toAdd;
}

template <class Type>
Type StackArray<Type>::remove()//good
{
	if (isEmpty())
		throw "EmptyStackException";
	//zmniejszanie tablicy
	auto toReturn = ArrayContainer[topIndex--];
	if(topIndex!=0)
		if (size / topIndex == 2)
			delargeStack();
	return toReturn;
}

template <class Type>
void StackArray<Type>::delargeStack()
{
	auto tmp = new Type[size / 2];
	for(int i=0;i<size/2;i++)
		tmp[i] = ArrayContainer[i]; //przepisanie do mniejszej tablicy
	delete[] ArrayContainer;
	ArrayContainer = tmp;
	size /= 2;
}


template <class Type>
void StackArray<Type>::enlargeStack()
{
	Type *tmp = new Type[size];
	for (int i = 0; i < size; i++)//przepisanie do tymczasowej tablicy
		tmp[i] = ArrayContainer[i];
	delete[] ArrayContainer;
	ArrayContainer = new Type[size * 2]; //nowa tablica o 2x wiekszym rozmiarze
	for (int i = 0; i < size; i++)//przepisanie do tymczasowej tablicy
		ArrayContainer[i] = tmp[i];//przepisanie z powrotem
	delete[]tmp; //usuniecie tymczzasowej tablicy
	size *= 2;
}


template <class Type>
Type* StackArray<Type>::getFirstPointer()
{
	return ArrayContainer;
}

template <class Type>
int StackArray<Type>::getStackSize()
{
	return topIndex;
}

template<class Type>
void showA(StackArray<Type>* stos)
{
	Type* tmp = stos->getFirstPointer();
	int i = 0;
	while (i < stos->getStackSize())
		std::cout <<*( tmp + i++)<<" ";
}
