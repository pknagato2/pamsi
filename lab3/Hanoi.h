#pragma once
#include "StackS.h"

template<class Type >
void moveDisc(StackS<Type>* fromPole, StackS<Type>* toPole)
{
	toPole->push(fromPole->pop());
}

template<class Type>
void moveTower(int height, StackS<Type>* fromPole, StackS<Type>* toPole, StackS<Type>* withPole)
{
	if (height >= 1)
	{
		moveTower(height - 1, fromPole, withPole, toPole);
		moveDisc(fromPole, toPole);
		moveTower(height - 1, withPole, toPole, fromPole);
	}
}

