#pragma once
#include "NodeS.h"
/*! \brief Klasa bazowa reprezentujaca liste jednokierunkowa.
*         
*  Wymagania: \n
*	-NodeS.h\n\n
*  Metody dla listy :\n
*  -frontAdd()\n
*  -backAdd()\n
*  -remove()\n
*  -removeAll()\n
*  -isEmpty()\n
*  -show()\n\n
*  
*  -------------Przyklad uzycia:-----------------------\n
*  ListS<int>* lista = new ListS;\n
*  lista->add(5);\n
*  lista->show();\n
*/
template<class Type>
class ListS
{
protected:
	virtual std::string throwEmptyContainerException() throw(std::string);  /*!< Wirtualna funkcja sluzaca do rzucania odpowienimi dla klas dziedziczacych wyjatkami. */
	NodeS<Type>*first;
	NodeS<Type>*last;
public:
	ListS();
	~ListS();
	bool isEmpty() const;
	ListS<Type>* frontAdd(Type toAdd);
	ListS<Type>* backAdd(Type toAdd);
	Type remove();
	void removeAll();
	NodeS<Type>* getPointerToFirst();
};

template <class Type>
std::string ListS<Type>::throwEmptyContainerException()
{
		throw "EmptyListException";
}

template <class Type>
ListS<Type>::ListS()
{
	first = nullptr;
	last = nullptr;
}

template <class Type>
ListS<Type>::~ListS()
{
	while(first!=nullptr)
		remove();
}

template <class Type>
bool ListS<Type>::isEmpty() const
{
	if (first == nullptr)
		return true;
	else
		return false;
}

template <class Type>
ListS<Type>* ListS<Type>::frontAdd(Type toAdd)
{
	NodeS<Type>* tmp = new NodeS<Type>;
	tmp->setElement(toAdd);
	tmp->setNext(first);
	first == nullptr ? last = tmp : NULL;
	first= tmp;

	return this;
}

template <class Type>
ListS<Type>* ListS<Type>::backAdd(Type toAdd)
{
	if (!isEmpty())
	{
		NodeS<Type>*tmp = new NodeS<Type>;
		tmp->setElement(toAdd);
		tmp->setNext(nullptr);
		last->setNext(tmp);
		last = tmp;
	}
	else
		frontAdd(toAdd);
	return this;
}

template <class Type>
Type ListS<Type>::remove()
{
	if (!isEmpty())
	{
		Type toReturn = first->getElement();
		NodeS<Type>*tmp = first;
		if (first->getNext() != nullptr)
			first = first->getNext();
		else {
			first = nullptr;
			last = nullptr;
		}
		delete tmp;
		return toReturn;
	}
	throwEmptyContainerException();
		
}

template <class Type>
void ListS<Type>::removeAll()
{
	while (first != nullptr)
		remove();
}

template <class Type>
NodeS<Type>* ListS<Type>::getPointerToFirst()
{
	return first;
}

template<class Type>
void showS(ListS<Type>& listS)
{
	NodeS<Type>* tmp = listS.getPointerToFirst();
	while(tmp!=nullptr)
	{
		std::cout << tmp->getElement();
		tmp = tmp->getNext();
	}
}