#pragma once
#include <string>
#include "DequeS.h"

bool isPal(std::string testStr)
{
	if (testStr.size()>1 && (*(testStr.begin()) == (*(testStr.end() - 1))))
	{
		return isPal(testStr.substr(1, testStr.size() - 2));
	}
	else if (testStr.size() == 1 || testStr.size() == 0)
		return true;
	else
		return false;

}

bool isPal(DequeS<std::string> kolejka)//co� jest nie tak ale nie wiem co :<
{
	if (kolejka.size() > 1 && kolejka.getFirstElem() == kolejka.getLastElem())
	{
		kolejka.remove();
		kolejka.removeBack();
		return isPal(kolejka);
	}
	else if (kolejka.size() == 1 || kolejka.size() == 0)
		return true;
	else
		return false;
}