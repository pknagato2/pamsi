#pragma once
#include <iostream>
#include <string>
#include "permutation.h"
#include "isPal.h"
#include "ListS.h"
#include "DequeS.h"
#include "QueueS.h"

using namespace std;


template<class Type>
void listaOpcje(ListS<Type>*lista)
{
	int opcja = 1;
	while (opcja)
	{
		cout << "1. Dodaj na przod listy\n2. Dodaj na tyl listy\n3. Usun z listy\n4. Usun wszystko z listy\n5. Pokaz liste\n6. Sprawdz czy lista jest pusta\n0. KONIEC.\n";
		cin >> opcja;
		switch (opcja)
		{
		case 1:
		{
			Type toAdd;
			cout << "Co chcesz dodac?\n";
			cin >> toAdd;
			lista->frontAdd(toAdd);
		}break;
		case 2:
		{
			Type toAdd;
			cout << "Co chcesz dodac?\n";
			cin >> toAdd;
			lista->backAdd(toAdd);
		}break;
		case 3:
		{
			Type removed;
			cout << "Usunieto: ";
			try {
				removed = lista->remove();
			}
			catch (char*msg)
			{
				cout << msg;
			}
			cout << removed << "\n";
		}break;
		case 4:
		{
			lista->removeAll();
		}break;
		case 5:
		{
			showS(*lista);
		}break;
		case 6:
		{
			if (lista->isEmpty())
				cout << "Lista jest pusta\n";
			else
				cout << "Lista nie jest pusta\n";
		}break;
		default:
		{
			cout << "Wybrano zle polecenie.\n";
		}break;
		}
	}
}

template<class Type>
void kolejkaOpcje(QueueS<Type>*kolejka)
{
	int opcja = 1;
	while (opcja)
	{
		cout << "1. Dodaj do kolejki\n2. Usun z kolejki\n3. Usun wszystko z kolejki\n4. Pokaz kolejke\n5. Sprawdz czy kolejka jest pusta\n0. KONIEC.\n";
		cin >> opcja;
		switch (opcja)
		{
		case 1:
		{
			Type toAdd;
			cout << "Co chcesz dodac?\n";
			cin >> toAdd;
			kolejka->backAdd(toAdd);
		}break;
		case 2:
		{
			Type removed;
			cout << "Usunieto: ";
			try {
				removed = kolejka->remove();
			}
			catch (char*msg)
			{
				cout << msg;
			}
			cout << removed << "\n";
		}break;
		case 3:
		{
			kolejka->removeAll();
		}break;
		case 4:
		{
			showS(*kolejka);
		}break;
		case 5:
		{
			if (kolejka->isEmpty())
				cout << "Kolejka jest pusta\n";
			else
				cout << "Kolejka nie jest pusta\n";
		}break;
		default:
		{
			cout << "Wybrano zle polecenie.\n";
		}break;
		}
	}
}

template<class Type>
void dkolejkaOpcje(DequeS<Type>*kolejka)
{
	int opcja = 1;
	while (opcja)
	{
		cout << "1. Dodaj na przod\n2. Dodaj na tyl\n3. Usun z przodu \n4. Usun z tylu\n5. Usun wszystko\n6. Pokaz\n7. Sprawdz czy jest pusta\n0. KONIEC.\n";
		cin >> opcja;
		switch (opcja)
		{
		case 1:
		{
			Type toAdd;
			cout << "Co chcesz dodac?\n";
			cin >> toAdd;
			kolejka->frontAdd(toAdd);
		}break;
		case 2:
		{
			Type toAdd;
			cout << "Co chcesz dodac?\n";
			cin >> toAdd;
			kolejka->backAdd(toAdd);
		}break;
		case 3:
		{
			Type removed;
			cout << "Usunieto: ";
			try {
				removed = kolejka->remove();
			}
			catch (char*msg)
			{
				cout << msg;
			}
			cout << removed << "\n";
		}break;
		case 4:
		{
			Type removed;
			cout << "Usunieto: ";
			try {
				removed = kolejka->removeBack();
			}
			catch (char*msg)
			{
				cout << msg;
			}
			cout << removed << "\n";
		}break;
		case 5:
		{
			kolejka->removeAll();
		}break;
		case 6:
		{
			showS(*kolejka);
		}break;
		case 7:
		{
			if (kolejka->isEmpty())
				cout << "Kolejka jest pusta\n";
			else
				cout << "Kolejka nie jest pusta\n";
		}break;
		default:
		{
			cout << "Wybrano zle polecenie.\n";
		}break;
		}
	}
}

void menuLista()
{
	int jakaLista;
	bool wybrana = false;
	while (!wybrana)
	{
		cout << "Wybierz typ listy:\n1. Lista intow\n2. Lista floatow\n3. Lista string�w.\n";
		cin >> jakaLista;
		if (jakaLista == 1)
		{
			ListS<int>* lista = new ListS<int>;
			listaOpcje(lista);
			delete lista;

		}
		else if (jakaLista == 2)
		{
			ListS<float>* lista = new ListS<float>;
			listaOpcje(lista);
			delete lista;
		}
		else if (jakaLista == 3)
		{
			ListS<string>* lista = new ListS<string>;
			listaOpcje(lista);
			delete lista;
		}
		else if (jakaLista == 0)
			break;
	}


}
//POLIMORFIZM
void menuKolejka()
{
	int jakaKolejka;
	bool wybrana = false;
	while (!wybrana)
	{
		cout << "Wybierz typ kolejki:\n1. Kolejka intow\n2. Kolejka floatow\n3. Kolejka string�w.\n";
		cin >> jakaKolejka;
		if (jakaKolejka == 1)
		{
			QueueS<int>* Kolejka = new QueueS<int>;
			kolejkaOpcje(Kolejka);
			delete Kolejka;

		}
		if (jakaKolejka == 1)
		{
			QueueS<float>* Kolejka = new QueueS<float>;
			kolejkaOpcje(Kolejka);
			delete Kolejka;
		}
		if (jakaKolejka == 1)
		{
			QueueS<string>* Kolejka = new QueueS<string>;
			kolejkaOpcje(Kolejka);
			delete Kolejka;
		}
		else if (jakaKolejka == 0)
			break;
	}


}

void menuDKolejka()
{
	int jakaKolejka;
	bool wybrana = false;
	while (!wybrana)
	{
		cout << "Wybierz typ kolejki:\n1. Kolejka intow\n2. Kolejka floatow\n3. Kolejka string�w.\n";
		cin >> jakaKolejka;
		if (jakaKolejka == 1)
		{
			DequeS<int>* Kolejka = new DequeS<int>;
			dkolejkaOpcje(Kolejka);
			delete Kolejka;
		}
		if (jakaKolejka == 1)
		{
			DequeS<float>* Kolejka = new DequeS<float>;
			dkolejkaOpcje(Kolejka);
			delete Kolejka;
		}
		if (jakaKolejka == 1)
		{
			DequeS<string>* Kolejka = new DequeS<string>;
			dkolejkaOpcje(Kolejka);
			delete Kolejka;
		}
		else if (jakaKolejka == 0)
			break;
	}


}

void menu()
{
	int opcja = 1;
	while (opcja)
	{
		cout << "1. Permutacje\n2. Lista\n3. Kolejka\n4. Kolejka z dwoma ko�cami\n0. KONIEC.\n";
		cin >> opcja;
		switch (opcja)
		{
		case 1:
		{
			std::vector<std::string>* PalTab = new std::vector<std::string>;
			string slowo;
			cout << "Podaj slowo: ";
			cin >> slowo;
			Permutation(slowo, slowo.size(), 0, PalTab);
			deleteDuplications(PalTab);
			for (auto x : *PalTab)
				cout << x << endl;
		}break;
		case 2:
		{
			menuLista();
		}break;
		case 3:
		{
			menuKolejka();
		}break;
		case 4:
		{
			menuDKolejka();
		}break;
		default:
		{

		}break;
		}
	}

}

