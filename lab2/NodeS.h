#pragma once
/*! \brief Klasa reprezentujaca wezel dla listy jednokierunkowej.
*
*  Wymagania: \n
*	-
*	\n
*  Metody dla wezla :\n
*  -setNext()\n
*  -getNext()\n
*  -setElement()\n
*  -getElement()\n
*  -show()\n
*
*  -------------Przyklad uzycia:-----------------------\n
*  NodeS<int>* wezel = new NodeS;\n
*  wezel->setElement(5);\n
*  wezel->setNext(nullptr);
*  wezel->show();\n
*/

#include <iostream>
template<class Type>
class NodeS final
{
	Type element;
	NodeS<Type>* next;
public:
	NodeS();
	~NodeS();
	void show()
	{
		std::cout << element<<std::endl;
		//std::cout << "Element: " << element << "\nPointer: " << next << std::endl;
	}
	Type getElement();
	NodeS<Type>* getNext();
	void setElement(Type toSet);
	void setNext(NodeS<Type>* toSet);
};

template <class Type>
NodeS<Type>::NodeS()
{
	next = nullptr;
}

template <class Type>
NodeS<Type>::~NodeS()
{
}

template <class Type>
Type NodeS<Type>::getElement()
{
	return element;
}

template<class Type>
NodeS<Type>* NodeS<Type>::getNext()
{
	return next;
}

template <class Type>
void NodeS<Type>::setElement(Type toSet)
{
	element = toSet;
}

template <class Type>
void NodeS<Type>::setNext(NodeS<Type>* toSet)
{
	next = toSet;
}
