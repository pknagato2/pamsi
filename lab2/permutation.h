#pragma once
#include "isPal.h"
#include <vector>
#include <algorithm>


void deleteDuplications(std::vector<std::string>*palTab)
{
	int sizz = palTab->size()-1;
	if(palTab!=nullptr)
		for (int i = 0; i <sizz; sizz = palTab->size() - 1)
		{
			if ((*palTab)[i] == (*palTab)[i + 1])
			palTab->erase(palTab->begin() + i);
			else i++;
			
		}
}

/*! \brief Funkcja generujaca permutacje lancucha znakowego
*
* 
*/

void Permutation(std::string word, unsigned int wordSize,unsigned int pos1 = 0, std::vector<std::string>*palTab = nullptr, std::vector<std::string>* permVector=nullptr)
{
	if (pos1 == wordSize)
	{
		if(palTab!=nullptr)
			if (isPal(word))
			{
				palTab->push_back(word);
				std::sort(palTab->begin(), palTab->end());
			}
		if (permVector != nullptr)
			permVector->push_back(word);
	}
	else
		for(unsigned i=pos1;i<wordSize;i++)
			{
				std::swap(word[pos1], word[i]);
				Permutation(word, wordSize, pos1 + 1,palTab,permVector);
				std::swap(word[pos1], word[i]);
			}
}