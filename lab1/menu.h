#pragma once
#include <string>
#include <iostream>
#include "Array.h"
#include "factorialR.h"
#include "powerR.h"
#include "isPal.h"

using namespace std;

inline bool menuTablica()
{
	int option = 1;
	Array* ptrTablica = nullptr;
	int wersjaTablicy = 0;

	while (option)
	{
		std::cout << "MENU TABLICA" << std::endl
			<< "1. Swtorz tablice" << std::endl
			<< "2. Wypelnij tablice losowymi wartosciami" << std::endl
			<< "3. Wyswietl tablice" << std::endl
			<< "4. Wyswietl najwiekszy element tablicy" << std::endl
			<< "5. Wyswietl wymiar tablicy" << std::endl
			<< "6. Zapisz tablice do pliku" << std::endl
			<< "7. Wczytaj tablice z pliku" << std::endl
			<< "8. Zapisz tablice do pliku binarnego" << std::endl
			<< "9. Wczytaj tablice z pliku binarnego\nUwaga! Tablica bedzie 1D." << std::endl;

		std::cin >> option;
		switch (option)
		{

		case 1:
		{
			while (wersjaTablicy != 1 && wersjaTablicy != 2)
			{
				std::cout << "Program dziala na tablicy N(1) lub tablicy NxM(2)\nKtora tablice stworzyc? ";
				std::cin >> wersjaTablicy;
				if (wersjaTablicy == 1)
				{
					int n;
					std::cout << "Podaj N: ";
					std::cin >> n;
					if (ptrTablica == nullptr)
						ptrTablica = new Array(n);
					else
					{
						delete ptrTablica;
						ptrTablica = new Array(n);
					}
				}
				else if (wersjaTablicy == 2)
				{
					int n, m;
					std::cout << "Podaj N: ";
					std::cin >> n;
					std::cout << "Podaj M: ";
					std::cin >> m;
					if (ptrTablica == nullptr)
						ptrTablica = new Array(n, m);
					else
					{
						delete ptrTablica;
						ptrTablica = new Array(n, m);
					}
				}
				else std::cerr << "BLAD!" << std::endl;
			}
			wersjaTablicy = 0;
		}break;

		case 2:
		{
			if (ptrTablica != nullptr)
			{
				int x;
				std::cout << "Program wypelnia tablice losowymi wartosciami od 0 do X.\nPodaj X: ";
				std::cin >> x;
				(*ptrTablica).fillRand(x);
			}
			else cerr << "Nie stworzono tablicy!\n";
		}break;
		case 3: if (ptrTablica != nullptr)(*ptrTablica).show();else cerr << "Nie stworzono tablicy!\n"; break;
		case 4: if (ptrTablica != nullptr)std::cout << (*ptrTablica).max() << std::endl; else cerr << "Nie stworzono tablicy!\n"; break;
		case 5: if (ptrTablica != nullptr)std::cout << (*ptrTablica).demention() << std::endl; else cerr << "Nie stworzono tablicy!\n"; break;
		case 6:
			{
			if (ptrTablica != nullptr)
			{
				std::string fileName;
				std::cout << "Podaj nazwe pliku: ";
				std::cin >> fileName;
				(*ptrTablica).saveToText(fileName);
			}
			else cerr << "Nie stworzono tablicy!\n";
		}break;
		case 7:
		{
			if (ptrTablica != nullptr)
			{
				std::string fileName;
				std::cout << "Podaj nazwe pliku: ";
				std::cin >> fileName;
				(*ptrTablica).readFromText(fileName);
			}
			else cerr << "Nie stworzono tablicy!\n";
		}break;
		case 8:
		{
			if (ptrTablica != nullptr)
			{
				std::string fileName;
				std::cout << "Podaj nazwe pliku: ";
				std::cin >> fileName;
				(*ptrTablica).saveToBin(fileName);
			}
			else cerr << "Nie stworzono tablicy!\n";
		}break;
		case 9:
		{
			if (ptrTablica != nullptr)
			{
				std::string fileName;
				std::cout << "Podaj nazwe pliku: ";
				std::cin >> fileName;
				if(!(*ptrTablica).readFromBin(fileName))
					cerr<<"Nie ma takiego pliku!"<<endl;
			}
			else cerr << "Nie stworzono tablicy!\n";
		}break;
		default:break;
		}
	}
	return true;
}


inline void menuGlowne()
{
	int option = 1;
	while (option)
	{
		std::cout << "MENU GLOWNE" << std::endl
			<< "1. Potega" << std::endl
			<< "2. Silnia" << std::endl
			<< "3. Tablica" << std::endl
			<< "4. Palindrom" << std::endl
			<< "0. KONIEC" << std::endl;
		std::cin >> option;
		switch (option)
		{
		case 1:
		{
			int x, p;
			std::cout << "Program oblicza x^p dla liczb dodatnich.Dla p<0 zwraca -1.\nPodaj x:";
			std::cin >> x;
			std::cout << "Podaj p:";
			std::cin >> p;
			std::cout << x << "^" << p << " = " << Power(x, p) << std::endl;
		}break;

		case 2:
		{
			int x;
			std::cout << "Program oblicza x!.\nPodaj x:";
			std::cin >> x;
			std::cout << x << "! = " << Factorial(x) << std::endl;
		}break;

		case 3:
		{
			menuTablica();
		}break;
		case 4:
		{
			std::string wyraz;
			std::cout << "Program sprawdza czy wyraz jest palindromem.\nPodaj wyraz: ";
			std::cin >> wyraz;
			std::cout << wyraz << (isPal(wyraz) ? "" : " nie") << " jest palindromem" << std::endl;
		}break;
		default:;
		}
	}
}



