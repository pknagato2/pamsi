#pragma once
inline int Power(int x, int p)
{
	if (p < 0)return -1;
	else if (p == 0)
		return 1;
	else if (p == 1)
		return x;
	else
		return x*Power(x, p - 1);
}
