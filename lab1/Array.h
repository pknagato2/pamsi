#ifndef ARRAY_H
#define ARRAY_H
#include <string>


class Array
{
    public:
		Array(int n);
        Array(int n,int m);
        ~Array();
        Array& fillNull();
        void show() const;
        void fillRand(int x);
        int max() const;
		int demention()const;
		bool saveToText(std::string fileName)const;
		bool saveToBin(std::string fileName)const;
		bool readFromText(std::string fileName);
		bool readFromBin(std::string fileName);
		int size()const;
    protected:
    private:
        int N,M;
		int * ArrayD1;
        int ** ArrayD2;
};

#endif // ARRAY_H
