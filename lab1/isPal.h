#pragma once
#include <string>
inline bool isPal(std::string testStr)
{
	if (testStr.size()>1 && (*(testStr.begin()) == (*(testStr.end() - 1))))
	{
		return isPal(testStr.substr(1, testStr.size() - 2));
	}
	else if (testStr.size() == 1 || testStr.size() == 0)
		return true;
	else
		return false;

}