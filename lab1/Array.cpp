#include "Array.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>

Array::Array(int n)
{
	N = n;
	M = 0;
	ArrayD2 = nullptr;
	ArrayD1 = new int[N];
}

Array::Array(int n,int m)
{
	ArrayD1 = nullptr;
    N=n;
    M=m;
   ArrayD2 = new int * [n];
    for(int i=0;i<N;++i)
        ArrayD2[i]=new int[M];
}

Array& Array::fillNull()
{
	if (ArrayD1 == nullptr)
	{
		for (int i = 0; i<N; i++)
			for (int j = 0; j<M; j++)
				ArrayD2[i][j] = 0;
	}
	else if (ArrayD2 == nullptr)
	{
		for (int i = 0; i<N; i++)
			ArrayD1[i] = 0;
	}
	return *this;
}

void Array::show() const
{
       
	   if (ArrayD1 == nullptr)
	   {
		   for (int i = 0; i<N; i++)
		   {
			   for (int j = 0; j<M; j++)
				   std::cout << ArrayD2[i][j] << " ";
			   std::cout << std::endl;
		   }
	   }
	   else if (ArrayD2 == nullptr)
	   {
		for (int i = 0; i<N; i++)
			std::cout << ArrayD1[i]<< " ";
		std::cout << std::endl;
		   
	   }
}

void Array::fillRand(int x)
{
	srand(time(0));
	if (ArrayD1 == nullptr)
	{
		for (int i = 0; i < N; i++)
			for (int j = 0; j < M; j++)
				ArrayD2[i][j] = rand() % (x + 1);
	}
	else if (ArrayD2 == nullptr)
	{
		for (int i = 0; i < N; i++)
			ArrayD1[i] = rand() % (x + 1);
	}
}
  
int Array::max() const
{
    int Max=0;
	if(ArrayD1==nullptr)
	{
		for (int i = 0; i<N; i++)
			for (int j = 0; j<M; j++)
				if (ArrayD2[i][j] >= Max)
					Max = ArrayD2[i][j];
	}
	else if (ArrayD2==nullptr)
	{
		for (int i = 0; i<N; i++)
			if (ArrayD1[i] >= Max)
				Max = ArrayD1[i];
	}
    
    return Max;
}

int Array::demention() const
{
	if (ArrayD1 == nullptr)
		return 2;
	else if (ArrayD2 == nullptr)
		return 1;
	else
		return 0;
}
bool Array::saveToText(std::string fileName) const
{
		std::ofstream fileToSave(fileName,std::ios::out);
		if (fileToSave.is_open())
		{
			if (ArrayD2 == nullptr)
			{
				for (int i = 0; i < N; ++i)
					fileToSave << ArrayD1[i] << " ";
			}
			
			else if (ArrayD1 == nullptr)
			{
				for (int i = 0; i < N; ++i)
				{
					for (int j = 0; j < M; ++j)
						fileToSave << ArrayD2[i][j] << " ";
					fileToSave << std::endl;
				}
			}
			fileToSave.close();
			fileToSave.clear();
			return true;
		}
		else
			return false;	
}

bool Array::saveToBin(std::string fileName) const
{
	std::ofstream fileToSave(fileName + ".bin", std::ios::out | std::ios::binary);
	if(fileToSave.is_open())
	{
		if(ArrayD2==nullptr)
		{
			for (int i = 0; i < N; ++i)
				fileToSave.write((char*)&ArrayD1[i], sizeof(ArrayD1[i]));
		}
		else if (ArrayD1==nullptr)
		{
			int tmp;
			for (int i = 0; i < N; ++i)
				for(int j=0;j<M;++j)
				{
					tmp =ArrayD2[i][j];
					fileToSave.write((char*)&tmp, sizeof(ArrayD1[i]));
				}
		}
		fileToSave.close();
		fileToSave.clear();
		return true;
	}
	else return false;
}

int Array::size() const
{
	if (ArrayD1 == nullptr)
		return N*M;
	else
		return N;
}
Array::~Array()
{
	if (ArrayD1 == nullptr)
	{
		for (int i = 0; i < N; ++i)
			delete[] ArrayD2[i];
		delete[] ArrayD2;
	}
	else if (ArrayD2==nullptr)
		delete[] ArrayD1;
}

bool Array::readFromText(std::string fileName)
{
	std::ifstream readFile(fileName);
	if(readFile.is_open())
	{
		int sizeBuff = 100;
		int* buffer=new int [sizeBuff];
		int i = 0;
		int m = 0;
		char tmpc;
		bool isEOF = false;

		std::ifstream sizeTest(fileName);
		if (sizeTest.is_open())
		{
			while ((tmpc = sizeTest.get()) != EOF)
				if (tmpc == '\n')
					++m;
			sizeTest.close();
			sizeTest.clear();
		}

		while (readFile >> buffer[i])
		{
			++i;
			if (i==sizeBuff)
			{
				sizeBuff *= 10;
				int*tmpBuff = buffer;
				buffer= new int[sizeBuff];
				for (int i = 0; i < (sizeBuff / 10); ++i)
					buffer[i] = tmpBuff[i];
				delete[] tmpBuff;
			}
		}
		
		
		//usuewanie poprzedniej zawartości
		if(ArrayD1!=nullptr)
		{
			delete[]ArrayD1;
			ArrayD1 = nullptr;
		}
		if(ArrayD2!=nullptr)
		{
			for (int i = 0; i < N; ++i)
				delete[] ArrayD2[i];
			delete[] ArrayD2;
			ArrayD2 = nullptr;
		}
		if(m==0)
		{
			ArrayD1 = new int[i];
			for (int j = 0; j < i; ++j)
				ArrayD1[j] = buffer[j];
			N = i;
		}
		else
		{
			int c = m;
			i = i / m;
			m = i;
			i = c;
			ArrayD2 = new int *[i];
			for (int j = 0; j<i; ++j)
				ArrayD2[j] = new int[m];
			
			for (int j = 0; j < i; ++j)
				for (int k = 0; k < m; ++k)
					ArrayD2[j][k] = buffer[m*j + k];

			
			N = i;
			M = m;
		}
		readFile.close();
		readFile.clear();
		return true;		
	}
	else return false;
}

bool Array::readFromBin(std::string fileName)
{
	std::ifstream sizeCheck(fileName+".bin", std::ios::binary|std::ios::ate);
	std::fstream::pos_type size = sizeCheck.tellg();
	size = size / sizeof(int);
	sizeCheck.close();

	std::ifstream fileToRead(fileName + ".bin", std::ios::binary);
	if (fileToRead.is_open())
	{
		ArrayD1 = new int[size];
		ArrayD2 = nullptr;
		for (int i = 0; i < size; ++i)
		{
			fileToRead.read((char*)&ArrayD1[i], sizeof(ArrayD1[i]));
		}
		N = size;
		fileToRead.close();
		fileToRead.clear();
		return true;
	}
	return false;
}