#pragma once

inline int Factorial(int n)
{
	if (n == 1 || n == 0)
		return 1;
	else if (n < 0)
		return 0;
	else
		return n*Factorial(n - 1);
}